# Hive Data Types and Create, Drop Database

## Data types in Hive
 **Data types** are very important elements in Hive query language and data modeling. for defining the table column types we must have to know about the data types and its usage.

some of data types in Hive:

- Numeric types
- String types
- Date/Time types
- Complex types


![](assets/markdown-img-paste-20181001164150427.png)

![](assets/markdown-img-paste-20181001164159319.png)

![](assets/markdown-img-paste-20181001164210561.png)

![](assets/markdown-img-paste-20181001164219470.png)

## Creation and dropping of Database in Hive
**Syntax**
`Create database "database_name"`
**Syntax**
`Drop database "database_name"`
