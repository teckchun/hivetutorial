# Apache Hbase and Hive Integration

Hbase is one of the most popular NOSQL database which runs on top of Hadoop ecco-system. In this manual, we will show you how to write data into Hbase tables using Hive.


## Create table in Hbase

```
 create 'employee','emp_details'
```


![](assets/markdown-img-paste-2018100914505731.png)

## Put some data

```
put 'employee',1,'emp_details:first_name','Debra'
put 'employee',1,'emp_details:last_name','Burke'
put 'employee',1,'emp_details:email','dburke0@unblog.fr'

```

## Create an external table in Hive

as we have already created a table in Hbase, so we need to create an external table in Hive referring to Hbase table.

```
CREATE EXTERNAL TABLE employee(
  id string COMMENT 'from deserializer',
  first_name string COMMENT 'from deserializer',
  last_name string COMMENT 'from deserializer',
  email string COMMENT 'from deserializer')
ROW FORMAT SERDE
  'org.apache.hadoop.hive.hbase.HBaseSerDe'
STORED BY
  'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES (
  'hbase.columns.mapping'=':key,emp_details:first_name,emp_details:last_name,emp_details:email',
  'serialization.format'='1')
TBLPROPERTIES (
  'hbase.table.name'='employee'
) ;

```
As we can see the statement above we use the `ROW FORMAT SERDE` from `org.apache.hadoop.hive.hbase.HBaseSerDe`, `STORED BY` from `org.apache.hadoop.hive.hbase.HBaseStorageHandler`,`WITH SERDEPROPERTIES (
  'hbase.columns.mapping'=':key,emp_details:first_name,emp_details:last_name,emp_details:email',
  'serialization.format'='1')
TBLPROPERTIES (
  'hbase.table.name'='employee'
) `

In order to make these work we have to do some configuration in Hive's `hive-site.xml` file by add the following lines to refer to jar library file for Hive to integrate with Hbase:
```
 <property>
        <name>hive.aux.jars.path</name>
        <value>file:///home/hadoop/hive/lib/hive-hbase-handler-2.3.3.jar,
file:///home/hadoop/hbase/lib/hbase-client-2.1.0.jar,
file:///home/hadoop/hbase/lib/hbase-server-2.1.0.jar,
file:///home/hadoop/hbase/lib/hbase-protocol-2.1.0.jar
        </value>
</property>

```

- Run the create table command and below are the results:


![](assets/markdown-img-paste-20181009145625614.png)


This is how we can reate a reference table for Hbase table using Hive.
Now let's insert data into this Hive table which in turn will get reflectged in Hbase table .



## Insert values into Hbase table through hive

In order to insert data into Hbase table through Hive, you need to specify the Hbase table name in the hive shell by using the below property before running the insert command:

```
set  hbase.mapred.output.outputtable=employee;
```
if it is not properly set you will get an error as shown below:
```
Job Submission failed with exception 'java.io.IOException(org.apache.hadoop.Hive.ql.metadata.HiveException: java.lang.IllegalArgumentException: Must specify table name)'
FAILED: Execution Error, return code 1 from org.apache.hadoop.Hive.ql.exec.mr.MapRedTask
```

Now let's insert one record into this Hive table using the below insert statement:

```

insert into table employee values('2','Robin','Harvey','rharvey1@constantcontact.com');

```

![](assets/markdown-img-paste-20181009150008238.png)

let's check for the same in the Hbase table:


![](assets/markdown-img-paste-20181009150029746.png)
This sums up the discussion on loading a record into Hbase using Hive.



Now let's implement to write the complete file(cluster of many records) into Hbase table using Hive.
The Hive table which referes to the Hbase table is non-native so we can not directly load the data into this table. if you try to do that , you will get the below exception:


```
FAILED: SemanticException [Error 10101]: A non-native table cannot be used as target for LOAD
```
for loading data in a file, we need to create a normal Hive table and then we need to write the insert overwrite statement.


## Loading the data in a file into Hbase using Hive

let's create a staging table for the employee table as show below :

```
CREATE TABLE employee_stg (
id STRING,
first_name STRING,
last_name STRING,
email STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
```
Load data from file into staging table :

![](assets/markdown-img-paste-20181009150359476.png)

Let's copy these contents into the employee table using insert overwrite statement.

```
insert overwrite table employee  select * from employee_stg;
```

below you can see that we have successfully written the data into the employee table:

![](assets/markdown-img-paste-20181009150529673.png)

let's check the data in Hbase :


![](assets/markdown-img-paste-20181009150544583.png)


So far we have seen how to write data into a table which is present in Hbase using Hive.


## Create table in Hbase from Hive


now let's see creating a Hbase table from Hive itself and inserting the data into that Hbase table.
```
CREATE TABLE employee1(
  id string COMMENT 'from deserializer',
  first_name string COMMENT 'from deserializer',
  last_name string COMMENT 'from deserializer',
  email string COMMENT 'from deserializer')
ROW FORMAT SERDE
  'org.apache.hadoop.hive.hbase.HBaseSerDe'
STORED BY
  'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES (
  'hbase.columns.mapping'=':key,emp_details:first_name,emp_details:last_name,emp_details:email',
  'serialization.format'='1')
TBLPROPERTIES (
  'hbase.table.name'='employee1'
) ;

```

the above query will create a Hive table with name `employee1` and also Hbase table with name `employee1`

let's insert some data to see the result for both table in Hbase and Hive.


`insert into table employee1 values('2','Robin','Harvey','rharvey1@constantcontact.com');`

- Result in Hive


![](assets/markdown-img-paste-2018100915105151.png)

- Result in Hbase


![](assets/markdown-img-paste-20181009151100361.png)


That's it for Hbase and Hive integration. Thank you!
