## Hive installation and configuration with MYSQL

- Hadoop up and running

### 3 steps processes:
1. installation of Hive
2. Hive shell commands
3. Install land configure MYSQL database


**1. Installation of Hive**

-Step1 download and install
http://apache.claz.org/hive/stable/

- go to the link and download .tar file

-Step2 extracting the file

`tar -xvf apache-hive-x.x.x.tar.gz`

-Step3 different configuration properties to be placed in Hive

1. placing Hive home path in bashrc file


![](assets/markdown-img-paste-20181001160344481.png)


![](assets/markdown-img-paste-20181001160349589.png)

2. placing Hadoop home path location in hive-config.sh


![](assets/markdown-img-paste-20181001160413610.png)

![](assets/markdown-img-paste-20181001160429498.png)

 -Step4 Creating Hive directories in hadoop:

 to communicate with Hadoop we need to create directories in Hadoop as show below:

 ![](assets/markdown-img-paste-20181001160703381.png)

Giving root permissions to create Hive folders in Hadoop.If it doesn't throw any error message, then it means that Hadoop has successfully given permissions to Hive folders.


![](assets/markdown-img-paste-20181001161137177.png)

- Step5 getting into hive shell by entering './hive'command

![](assets/markdown-img-paste-20181001161243712.png)

### Hive shell commands
- Create table


![](assets/markdown-img-paste-2018100116262569.png)
- from the above screen shot we can observe the following:

1. Creation of Sample Table with column names in Hive
- here the table name is  "product" with three column name product, pname, and price
- the three column names denoted by their respective data type
- all fields are terminated by coma ','

2. Displaying Hive Table information
- Using "describe" command we can able to see the table information present in Hive
- at the end, it will display time to perform this command and number of rows it fetched.

### install and configure MYSQL database

#### why use mysql in hive as meta store
- by default, hive comes with derby database as metastore.
- derby databased can support only single active user at a time
- derby is not recommended in production environment

- the solution is use MYSQL as Meta storage at backend to connect multiple users with Hive at a time
- MYSQL  is Best choice for the standalone metastore

#### steps to install MYSQL and configure MYSQL databased in Hive on Hadoop
1. Install mysql-server

![](assets/markdown-img-paste-20181001163204828.png)


2. install MYSQL Java connector.

![](assets/markdown-img-paste-20181001163224274.png)

3. create soft link for connector in Hive lib directory. this is soft link between Java and Mysql

![](assets/markdown-img-paste-2018100116330238.png)

4. configuring Mysql storage in Hive

![](assets/markdown-img-paste-20181001163321998.png)

5. creating username and password for Mysql granting privileges.

![](assets/markdown-img-paste-2018100116335949.png)

```
mysql> CREATE USER 'hiveuser'@'%' IDENTIFIED BY 'hivepassword';
mysql> GRANT all on *.* to 'hiveuser'@localhost identified by 'hivepassword';
mysql>  flush privileges;
```

6. Configuring `hive-site.xml`



![](assets/markdown-img-paste-2018100116350082.png)

![](assets/markdown-img-paste-20181001163507565.png)

**place this code in hive-site.xml**


`hive-site.xml`
```
<configuration>
	<property>
		<name>javax.jdo.option.ConnectionURL</name>
		<value>jdbc:mysql://localhost/metastore?createDatabaseIfNotExist=true</value>
		<description>metadata is stored in a MySQL server</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionDriverName</name>
		<value>com.mysql.jdbc.Driver</value>
		<description>MySQL JDBC driver class</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionUserName</name>
		<value>hiveuser</value>
		<description>user name for connecting to mysql server</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionPassword</name>
		<value>hivepassword</value>
		<description>password for connecting to mysql server</description>
	</property>
</configuration>

```
7. create table in Hive

![](assets/markdown-img-paste-2018100116373248.png)

8. entering into Mysql shell mode to check whether it is stored in Mysql or not


![](assets/markdown-img-paste-20181001163815647.png)
