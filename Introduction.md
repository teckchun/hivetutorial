## What is Hive? Archetecture and Modes

### What is Hive?
Hive is an ETL & Data warehousing tool devloped on top of HDFS. it makes job easy for perfoming operation such as :
- Data Encapsulation
- Ad-hoc queries
- Analysis of huge datasets


### important characteristics of hive
- tables and db are created first and then data is loaded into these tables
- hive as data warehouse designed for managing and querying only structured data that is stored in tables.
- while dealing with sturctured data , MR doesn't have optimization and usability features like UDFs but Hive frmaework does. query optimization.
- hive's SQL-inspired languages separat4es the user form the complexity of MR programming. it reuses familiear concepts from RDBMS such as tablees, rows columns ,schema.
- a new and important component i.e Metastore used for storing schema infomation. this Metastore typically resides in a relationsl database. we can interact with Hive using methods like
    - WebGUI
    - JDBC

- CLI tool to write query using Hive Query Language(HQL)
- HQL similar to SQL
    - select * from table_name
- Hive supports 4 file formats thos are TEXTFILE, SEQUENCEFILE,ORC , RCFILE(Record Columnar File)
- for single user metadata storage Hive uses derby databased and for multiple user Metadata or shared Metadata cased Hive uses MYSQL

** Key points of Hive:
-   the major difference between HQL and SQL is that Hive query executes on Hadoop's infrastructure rather than the traditional database.
- the Hive query execution is going to be like series of automatically generated map reduce Jobs.
- hive supports partition and buckets concepts for easy retrieval of data when the client executes the query
- hive support custom specific UDF(User Defined Function) for data cleaning , filtering, etc. based on the requirements of the programmers one can define Hive UDFs.

Hive Vs RDBMS

- with hive we can perform some peculiar functionality that RDBMS doesn't. for huge amount of data that is in peta-bytes, querying it and gettting result sin seconds is important. hive does this quite efficiently.

<b>Some key differences between Hive and RDBMS</b>

- Relational databases are of "Schema on READ and Schema on Write". First creating a table then inserting data into the particular table. On relational database tables, functions like Insertions, Updates, and Modifications can be performed. 

- Hive is "Schema on READ only". So, functions like the update, modifications, etc. don't work with this. Because the Hive query in a typical cluster runs on multiple Data Nodes. So it is not possible to update and modify data across multiple nodes.( Hive versions below 0.13)

- Also, Hive supports "READ Many WRITE Once" pattern. Which means that after inserting table we can update the table in the latest Hive versions. 

NOTE: However the new version of Hive comes with updated features. Hive versions ( Hive 0.14) comes up with Update and Delete options as new features 

