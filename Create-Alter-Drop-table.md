# Hive Create, Alter and Drop Table

**Example**
1. Creating table guru_sample with two column names such as "empid", "empname"
2. Displaying tables present in guru99 database
3. guru_sample displaying under tables
4. altering table "guru_sample" as "guru_sampleNew"
5. execute "show" command to check for changes.

![](assets/markdown-img-paste-20181001164629504.png)


`dropping table guru_sampleNew`

![](assets/markdown-img-paste-20181001164702846.png)

## Table types and its usage

coming to Tables it's just like the way that we can create in traditional RDBMS. the functionalities such as filtering, joins can be performed on the tables.
Hive deals with two types of table structures like **Internal and External** tables depending on the loading and design of schema in Hive.

**Internal Tables**

- internal table is tightly coupled in nature. in this type of table, first we have to create table and load the data.
- we can call this one as **data on schema**
- the stored location of this table will be at `/user/hive/warehouse`

**When to Choose Internal Table**
- if the processing data available in local file system
- if we want Hive to manage the complete lifecycle of data including the deletion

**sample code for internal table**

1. to create the internal table

```
CREATE TABLE test_internaltable(id int, name string);
ROW format delimited Fields terminated by '\t';
```

2. load the data into internal table
```
LOAD DATA INPATH 'path_of_file' INTO test_internaltable;
```
3. display the content of the table
```
select * from test_internaltable
```

4. to drop the internal table
```
DROP TABLE test_internaltable
```

**when dropping test_internaltable, including its metadata and its data will be deleteed from Hive**


**External tables**
- external table is loosely coupled in nature. Data will be available in HDFS. the table is going to create on HDFS data.
- in other way, we can say like its crating schema on data.
- at the time of dropping the table it drops only schema, the data will be still available in HDFS as before.
- external tables provide an option to create multiple schemas for the data stored in HDFS instead of deleting the data every time whenever schema updates

**when to choose external table**
- if processing data available in HDFS
- useful when the files are being used outside of Hive

**sample code for external table**
1. create external table
```
CREATE EXTERNAL TABLE test_external(id int, name string) row format delimited
fields terminated by '\t' LOCATION '/user/guru99hive/gurohive_external'
```
2. if we are not specifying the location at the time of table creation, we can load the data manually.

```
LOAD DATA INPATH '/user/guru99/data.txt' INTO TABLE test_external;
```
